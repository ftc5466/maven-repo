# Combustible Lemons' Maven Repository

This Git repository acts as a Maven repository for any projects we are distributing. Currently, this just contains our libraries for the Android FTC SDK, but more could potentially be added here in the future.

---

# Adding the Repository to Gradle

To add any packages in here as Gradle dependencies, you will first need to add this repository to your build.gradle (for the FTC SDK, this should be the FtcRobotController one):

```
repositories {
    ...
    maven {
        url 'https://bitbucket.org/ftc5466/maven-repo/raw/master/'
    }
}
```

---

# Current Projects

### FTCFiller

This library automatically fetches motors and sensors in an OpMode in the Robot Controller app for FTC.

```
compile 'com.ftc5466.ftcfiller:ftcfiller:0.2@aar'
```